Tools for working with FHIR packaged as an OCI container image

Containts:

- [**ig-publisher**](https://github.com/HL7/fhir-ig-publisher/) with a cli wrapper `fhir-ig-publisher`
- [**sushi**](https://github.com/FHIR/sushi) and [**GoFSH**](https://github.com/FHIR/GoFSH) for [FHIR shorthand](https://hl7.org/fhir/uv/shorthand/)
- [**Firely Terminal**](https://fire.ly/products/firely-terminal/)
