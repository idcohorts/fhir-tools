#!/bin/bash

export JAVA_TOOL_OPTIONS="$JAVA_TOOL_OPTIONS -Dfile.encoding=UTF-8"

exec java -jar /opt/fhir-ig-publisher.jar -ig . $txoption $*
