FROM debian:bookworm

# Install debian packages
COPY apt.txt .
RUN apt-get update && apt-get install -y $(sed -e 's/#.*//' -e '/^$/d' apt.txt | sort | uniq ) \
    && rm -rf /var/lib/apt/lists/*

# Install dotnet
RUN curl -L https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.deb -o /tmp/packages-microsoft-prod.deb && \
    dpkg -i /tmp/packages-microsoft-prod.deb && \
    rm /tmp/packages-microsoft-prod.deb && \
    apt-get update && apt-get install -y dotnet-sdk-6.0 && \
    rm -rf /var/lib/apt/lists/*

# # Install libssl1.1 (required by fhir-codegen)
# RUN curl -L https://deb.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.1_1.1.1n-0+deb11u5_amd64.deb -o /tmp/libssl1.1.deb && \
#     dpkg -i /tmp/libssl1.1.deb && \
#     rm /tmp/libssl1.1.deb

# Install IG Publisher
ARG FHIR_IG_PUBLISHER_VERSION=1.4.5
RUN curl -L https://github.com/HL7/fhir-ig-publisher/releases/download/${FHIR_IG_PUBLISHER_VERSION}/publisher.jar -o /opt/fhir-ig-publisher.jar
COPY fhir-ig-publisher-wrapper.sh /usr/local/bin/fhir-ig-publisher
RUN chmod +x /usr/local/bin/fhir-ig-publisher

# Install FSH tools
ARG FSH_SUSHI_VERSION=3.3.3
ARG GOFSH_VERSION=2.1.1
RUN npm install -g eslint-webpack-plugin fsh-sushi@${FSH_SUSHI_VERSION} gofsh@${GOFSH_VERSION}

# Install firely-terminal
ARG FIRELY_TERMINAL_VERSION=3.1.0
RUN dotnet tool install --tool-path /opt/firely-terminal --version ${FIRELY_TERMINAL_VERSION} firely.terminal
RUN ln -s /opt/firely-terminal/fhir /usr/local/bin/fhir

# # Install fhir-codegen
# ARG FHIR_CODEGEN_VERSION=1.0.0
# RUN curl -L https://github.com/microsoft/fhir-codegen/releases/download/v${FHIR_CODEGEN_VERSION}/fhir-codegen-v${FHIR_CODEGEN_VERSION}-linux-x64.tar.gz | tar -xzC /opt && \
#     mv /opt/fhir-codegen-v${FHIR_CODEGEN_VERSION}-linux-x64 /opt/fhir-codegen && \
#     ln -s /opt/fhir-codegen/fhir-codegen-cli /usr/local/bin/fhir-codegen-cli
# # fix error about missing "valid ICU package installed on the system."
# ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
